# Lab6 -- Mutation testing

![Pipeline](https://gitlab.com/Sh1co/lab-6-mutation-testing/badges/main/pipeline.svg)

## Mutatest report

```txt
2023-04-12 04:47:45,326: CLI Report:

Mutatest diagnostic summary
===========================
 - Source location: E:\Active projects\SQR labs\lab-6-mutation-testing\src
 - Test commands: ['pytest']
 - Mode: s
 - Excluded files: []
 - N locations input: 10
 - Random seed: None

Random sample details
---------------------
 - Total locations mutated: 10
 - Total locations identified: 21
 - Location sample coverage: 47.62 %


Running time details
--------------------
 - Clean trial 1 run time: 0:00:00.804511
 - Clean trial 2 run time: 0:00:00.797407
 - Mutation trials total run time: 0:00:34.599970

2023-04-12 04:47:45,329: Trial Summary Report:

Overall mutation trial summary
==============================
 - DETECTED: 40
 - TOTAL RUNS: 40
 - RUN DATETIME: 2023-04-12 04:47:45.326185
```

As you can see 100% (40/40) of mutants were killed, which is even better than all the soaps claiming that they kill 99.9% of bacteria.
