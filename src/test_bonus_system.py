from bonus_system import calculateBonuses


def test_valid_standard():
    amount = 1
    assert calculateBonuses("Standard", amount) == (0.5 * 1)
    amount = 10000
    assert calculateBonuses("Standard", amount) == (0.5 * 1.5)
    amount = 50000
    assert calculateBonuses("Standard", amount) == (0.5 * 2)
    amount = 100000
    assert calculateBonuses("Standard", amount) == (0.5 * 2.5)


def test_valid_premium():
    amount = 1
    assert calculateBonuses("Premium", amount) == (0.1 * 1)
    amount = 10000
    assert calculateBonuses("Premium", amount) == (0.1 * 1.5)
    amount = 50000
    assert calculateBonuses("Premium", amount) == (0.1 * 2)
    amount = 100000
    assert calculateBonuses("Premium", amount) == (0.1 * 2.5)


def test_valid_diamond():
    amount = 1
    assert calculateBonuses("Diamond", amount) == (0.2 * 1)
    amount = 10000
    assert calculateBonuses("Diamond", amount) == (0.2 * 1.5)
    amount = 50000
    assert calculateBonuses("Diamond", amount) == (0.2 * 2)
    amount = 100000
    assert calculateBonuses("Diamond", amount) == (0.2 * 2.5)


def test_invalid():
    amount = 1
    assert calculateBonuses("Utter nonsense", amount) == (0 * 1)
    amount = 10000
    assert calculateBonuses("zzzzzzzzzzzzzzzzzz", amount) == (0 * 1.5)
    amount = 50000
    assert calculateBonuses("a", amount) == (0 * 2)
    amount = 100000
    assert calculateBonuses(" ", amount) == (0 * 2.5)


def test_bonus_range():
    amount = 5000
    assert calculateBonuses("Standard", amount) == (0.5 * 1)
    amount = 20000
    assert calculateBonuses("Standard", amount) == (0.5 * 1.5)
    amount = 70000
    assert calculateBonuses("Standard", amount) == (0.5 * 2)
    amount = 150000
    assert calculateBonuses("Standard", amount) == (0.5 * 2.5)
